// base xAOD access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// ROOT
#include "TFile.h"

// std/stl
#include <iostream>

#include "hhbb4l/CLI11.hpp"


int main(int argc, char* argv[]) {

    std::string input_filepath{""};

    // parse the user input
    CLI::App app;
    app.add_option("-i,--input", input_filepath, "Input DAOD filepath")->required();
    CLI11_PARSE(app, argc, argv);

    // initialize xAOD environment and open the input file
    const char* algo = argv[0];
    RETURN_CHECK(algo, xAOD::Init());
    xAOD::TEvent event(xAOD::TEvent::kClassAccess);

    std::unique_ptr<TFile> ifile(TFile::Open(input_filepath.c_str(), "READ"));
    if(!ifile || ifile->IsZombie()) {
        throw std::logic_error("ERROR Could not open input file \"" + input_filepath + "\"");
    }

    RETURN_CHECK(algo, event.readFrom(ifile.get()));

    uint32_t n_entries = event.getEntries();
    std::cout << "Opened file with " << n_entries << " events" << std::endl;
    return 0;
}
